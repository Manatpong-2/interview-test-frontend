import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/@services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registrationForm: FormGroup;
  isSubmit: boolean = false;
  errorMsg: any = null;
  userObj = JSON.parse(localStorage.getItem("userData"));
  loading: boolean = false;
  imgUrl: string = null;
  submitErrorMsg: string = null;
  tmpImgName: string = null;
  constructor(
    private _router: Router,
    private _userService: UserService,
    private formBuilder: FormBuilder
  ) {
    this.registrationForm = this.formBuilder.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12),
        Validators.pattern('^[A-Za-z0-9_]*$')]),
      firstname: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
        Validators.pattern('^[A-Za-z]*$')]),
      lastname: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
        Validators.pattern('^[A-Za-z]*$')]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ]),
      confirm_password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ]),
      profile_img: [''],
    });

  }

  ngOnInit(): void {
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      let file_extension = file.name.split('.').pop();
      if (file_extension != 'jpg') {
        alert('Invalid file extension')
      } else {
        this.registrationForm.get('profile_img').setValue(file);
        const formData = new FormData();
        formData.append('profile_img', this.registrationForm.get('profile_img').value);
        this._userService.uploadImg(formData).subscribe((data) => {
          if (data['status'] == 1) {
            // this.imgUrl = data['data']['file_url']
            const url_str = String(window.location.href).split("/");
            const root_url = String(url_str[2]).split(":");
            this.imgUrl = url_str[0] + '//' + root_url[0] + '/images/' + data['data']['filename'];
          } else {
            alert(data['msg']);
          }
        })
      }
    }
  }

  register() {
    this.isSubmit = true;
    if (this.registrationForm.valid && (this.registrationForm.get('password').value == this.registrationForm.get('confirm_password').value)) {
      if (confirm('Do you want to confirm the registration?')) {
        this.errorMsg = null;
        this.loading = true;
        const formData = new FormData();
        formData.append('username', this.registrationForm.get('username').value);
        formData.append('firstname', this.registrationForm.get('firstname').value);
        formData.append('lastname', this.registrationForm.get('lastname').value);
        formData.append('password', this.registrationForm.get('password').value);
        if(this.registrationForm.get('profile_img').value) {
          formData.append('profile_img', this.registrationForm.get('profile_img').value);
        }
        if(this.tmpImgName) {
          formData.append('tmp_img_name', this.tmpImgName);
        }
        this._userService.register(formData).subscribe((data) => {
          if (data['status'] == 1) {
            this.registrationForm.get('username').setValue(null);
            this.registrationForm.get('firstname').setValue(null);
            this.registrationForm.get('lastname').setValue(null);
            this.registrationForm.get('password').setValue(null);
            this.registrationForm.get('confirm_password').setValue(null);
            this.registrationForm.get('profile_img').setValue(null);
            this.imgUrl = null;
            this.isSubmit = false;
          }
          this.loading = false;
          alert(data['msg']);
        })
      }
    } else {
      this.errorMsg = this.getFormValidationErrorMsg();
    }
  }

  isError(param: string) {
    if (this.isSubmit && this.registrationForm.get(param)?.errors) {
      return 'is-invalid';
    } else {
      return '';
    }
  }

  getParamErrorMsg(param) {
    const errors = this.registrationForm.get(param)?.errors;
    if (errors) {
      if ('minlength' in errors) {
        return 'The ' + param + ' must be at least ' + errors['minlength']['requiredLength'] + ' characters';
      } else if ('required' in errors) {
        return 'The ' + param + ' is required';
      } else if ('pattern' in errors) {
        return 'The ' + param + ' is invalid format';
      } else {
        return 'Unknown error';
      }
    }
    return '';

  }

  getFormValidationErrorMsg() {
    let errMsg = null;
    Object.keys(this.registrationForm.controls).some(key => {
      const errors = this.registrationForm.get(key)?.errors;
      if (errors) {
        if ('minlength' in errors) {
          errMsg = 'The ' + key + ' must be at least ' + errors['minlength']['requiredLength'] + ' characters';
        } else if ('required' in errors) {
          errMsg = 'The ' + key + ' is required';
        } else if ('pattern' in errors) {
          errMsg = 'The ' + key + ' is invalid format';
        } else {
          errMsg = 'Unknown error';
        }
        return true;
      }
    });
    return errMsg;

  }
}
