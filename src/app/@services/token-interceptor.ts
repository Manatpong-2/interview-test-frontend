import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userObj = JSON.parse(localStorage.getItem("userData"));
    const userToken = userObj ? userObj['token'] : '';
    const modifiedReq = req.clone({ 
      headers: req.headers.set('token', userToken),
    });
    return next.handle(modifiedReq);
  }
}