import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/@services/auth.service';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmit: boolean = false;
  errorMsg: any = null;
  userObj = JSON.parse(localStorage.getItem("userData"));
  loading: boolean = false;
  isCheckAuth: boolean = false;
  constructor(
    private _router: Router,
    private _authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12),
        Validators.pattern('^[A-Za-z0-9_]*$')]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12)
      ])
    });

  }

  ngOnInit(): void {
    this.checkAuth()
  }

  checkAuth() {
    if (this.userObj) {
      this._authService.authCheck(this.userObj['token']).subscribe((data) => {
        if (data['status'] == 1) {
        this._router.navigate(["/home"]);
        }
        this.isCheckAuth = true;
      })
    } else {
      this.isCheckAuth = true;
    }
  }

  login() {
    if (this.loginForm.valid) {
      this.errorMsg = null;
      this.loading = true;
      this._authService.login(this.loginForm.value).subscribe((data) => {
        if(data['status'] == 1) {
          localStorage.setItem("userData", JSON.stringify( data["data"] ));
          this._router.navigate(["/home"]);
        } else {
          this.errorMsg = data['msg'];
        }
        this.loading = false;
      })
    } else {
      this.errorMsg = this.getFormValidationErrorMsg();
    }
  }

  isError(param: string) {
    if(this.isSubmit && this.loginForm.get(param)?.errors) {
      return 'is-invalid';
    } else {
      return '';
    }
  }

  getFormValidationErrorMsg() {
    let errMsg = null;
    Object.keys(this.loginForm.controls).some(key => {
        const errors = this.loginForm.get(key)?.errors;
        if(errors) {
          if ('minlength' in errors) {
            errMsg = 'The ' + key + ' must be at least ' + errors['minlength']['requiredLength'] + ' characters';
          } else if ('required' in errors) {
            errMsg = 'The ' + key + ' is required';
          } else if ('pattern' in errors) {
            errMsg = 'The ' + key + ' is invalid format';
          } else {
            errMsg = 'Unknown error';
          }
          return true;
        }
    });
    return errMsg;

  }
}
