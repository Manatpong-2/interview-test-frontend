import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _api: string = environment._api;

  constructor(private _http: HttpClient) { }

  getUser(user_id: string): Observable<any> {
    return this._http.get<any>(this._api + "user/" + user_id).pipe(catchError(this.errorHandler))
  }

  uploadImg(params: any): Observable<any> {
    return this._http.post<any>(this._api + "user/image", params).pipe(catchError(this.errorHandler))
  }

  register(params: any): Observable<any> {
    return this._http.post<any>(this._api + "user", params).pipe(catchError(this.errorHandler))
  }

  updateProfile(user_id: number, params: any): Observable<any> {
    return this._http.post<any>(this._api + "user/" + user_id, params).pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    const key = Object.keys(error.error)[0];
    alert(error.error[key][0])
    return throwError(error.error[key][0] || 'Server error')
  }
}
