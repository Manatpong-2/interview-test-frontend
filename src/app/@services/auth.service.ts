import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _api: string = environment._api;

  constructor(private _http: HttpClient) { }
  
  login(params: object) {
    return this._http.post<any>(this._api + "auth/login", params).pipe(catchError(this.errorHandler))
  }

  logout(token: any) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: { 'token': token }
    };
    return this._http.delete<any>(this._api + "auth/logout", httpOptions).pipe(catchError(this.errorHandler))
  }

  authCheck(token: string) {
    return this._http.post<any>(this._api + "auth", { token: token }).pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
    const key = Object.keys(error.error)[0];
    alert(error.error[key][0])
    return throwError(error.error[key][0] || 'Server error')
  }
}
