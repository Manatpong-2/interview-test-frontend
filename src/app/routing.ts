import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { HomeComponent } from './@components/home/home.component';
// import { UserProfileComponent } from './@components/home/@list/user-profile/user-profile.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { LoginComponent } from './@layout/login/login.component';
import { HomeComponent } from './@layout/home/home.component';
import { ProfileComponent } from './@layout/profile/profile.component';
import { RegisterComponent } from './@layout/register/register.component';


const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: "register", component: RegisterComponent },
    {
        path: 'home',
        component: HomeComponent,
        children: [
            {
                path: "**",
                component: ProfileComponent,
                outlet: "home"
            },
            {
                path: "profile",
                component: ProfileComponent,
                outlet: "home"
            }]
    },

    // /**/
    { path: '**', component: LoginComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });